---
layout: page
title: Spenden
permalink: /spenden/
nav_order: 130
---

# Spenden
## Geldspenden
Wir sind über Spenden sehr dankbar und wissen diese sehr zu schätzen.

Wir finanzieren darüber Campmaterialien, Flyer, Plakate, Banner, gelegentlich
Klettermaterialien, juristische Ausgaben und diverse kleinere Posten.

Dankenswerterweise nimmt die [Bürgerstiftung
Augsburg](https://www.buergerstiftung-augsburg.de/) für uns Spenden entgegen.
Die Kontodaten lauten:

    Name: Bürgerstiftung Augsburg
    IBAN: DE22 7205 0000 0000 0263 69
    Verwendungszweck: Klimacamp (unbedingt angeben, sonst kann die Spende nicht zugeordnet werden!)

## Sachspenden
Es gibt es ein paar Dinge, die wir immer wieder gebraucht werden und die gerne jederzeit [im Camp](/mitmachen/#adresse) vorbei gebracht werden können.

_Aber_: Teil dieser Bewegung ist auch Kapitalismuskritik. Es ist nicht im Sinne von einigen der Aktivisti, wenn Leute Dinge, die auf der Liste stehen, einkaufen, um den Bedarf schnellst- und bestmöglich zu decken, vielmehr geht es Leuten hier darum, sich solidarisch zu organisieren, Materialien zu teilen, bereits vorhandene Dinge zur Verfügung zu stellen oder auch „nur“ den eigenen Überfluss an Leute weitergeben, die ihn benötigen.

### Werkzeuge und Baumaterial
- Akkuschrauber, Akkus, Zangen, Schraubschlüssel, Hammer, ...
- Holzbretter/-latten und Kanthölzer (so lang wie möglich)
- Paletten
- Planen (LKW-Planen, Werbebanner, etc.)
- Handkarren, Schubkarren,
- Metallstangen
- Zement
- Schrauben, Nägel, Metallwinkel, ...
- Fenster

### Demomaterial
- Sekundenkleber
- Glitzer
- Schwarze Jacken
- Regenschirme
- Sturmmasken

### Küchenutensilien
- Große Töpfe
- Geschirr (bruchfest)
- Großküchenbedarf
- **Nudelmaschine**

### Fahrradbedarf
- Schlösser
- Fahräder, Lastenräder & Anhänger
- Schläuche
- Werkzeug
- Pumpe

### Elektronik
- **Powerbanks**
- Laptops
- Handys
- Registrierte SIM-Karten
- **Kopflampen** (am besten mit Rotlicht)
- **Megaphone**
- Solarpanel


### Klettermaterialien
*Gebrauchtes Klettermaterial bitte immer beschriften mit Art, Kaufzeitpunkt, Nutzungsdauer und -intensität (Zettel dran, nicht direkt draufschreiben).*
- Karabiner
- Seilrollen
- Drahtseile
- Gurte und Seile

### Outdoor 
- **Hängematten**
- Sturmfeuerzeuge (nachfüllbar)
- stabile Trinkwasserkanister alle Größen (z. B. 20 l Bundeswehr-Kanister)
- **Rucksäcke**
- verschließbare Taschen
- Zelte
- Isomatten, Luftmatratzen (auch mit Löchern)
- **Schlafsäcke** und Decken
- Sturmmasken

### Kleidung 
- Gummistiefel
- Thermo-Unterwäsche
- Tarnkleidung (in Oliv, grün, braun, schwarz)
- Regenkleidung, Winterkleidung
- Schlauchschale (in schwarz,oliv)
- Winterschuhe
- Wintersocken
- Arbeitshandschuhe

