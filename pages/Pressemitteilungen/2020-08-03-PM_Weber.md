---
layout: page
title:  "03.08.2020: Von der Umsetzungslücke und Ambitionslücke von Augsburgs Stadtregierung"
date:   2020-08-03 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 6
---

*Pressemitteilung des Augsburger Klimacamps am 3. August 2020*

# Aktivist\*innen des Augsburger Klimacamps kritisieren scharf: „Oberbürgermeisterin Weber versteht die Klimakrise nicht.“ — von der Umsetzungslücke und Ambitionslücke von Augsburgs Stadtregierung

"Wir sind nachhaltigste Großstadt Deutschlands, wir sind der größte kommunale Waldbesitzer in Bayern, wir machen sehr viel. Und ich glaube, dass es wichtig ist, dass man in einer Diskussion nicht ganz aus den Augen verliert, was schon bereits getan ist." Diese Aussage gab Oberbürgermeisterin Weber in einem Interview des Bayerischen Rundfunks ab, das in der Abendschau anlässlich des einmonatigen Bestehens des Augsburger Klimacamps ausgestrahlt wurde [1].

"Ich verstehe nicht, wie Frau Weber nach all unserer Arbeit die Dringlichkeit und Schwere der Klimakrise immer noch nicht versteht", äußert sich Schülerin Eva Stoffels (16).

Die Bezeichnung „Nachhaltigste Großstadt Deutschlands“ scheint eine leere Phrase zu sein; laut einer Studie des IFW Kiel [2] von 2013 steht Augsburg in der Kategorie Nachhaltigkeit im deutschen Städtevergleich auf Platz 29. Auch verfehlte die Stadt immer wieder die selbstgesetzten Fünfjahresziele zur CO2-Einsparung — dies gelang zum ersten Mal 2019. Aber nicht nur das, denn die vorgesehene Reduktion um 10% in fünf Jahren ist bei Weitem nicht ausreichend, um die Klimakrise abzuwenden. Die Stadtregierung wird in fünf Jahren — noch in dieser Legislatur — das Augsburg zustehende CO2-Budget vollständig aufgebraucht haben. Bis 2050 wird sie laut den Zielen im Koalitionsvertrag 34 Millionen Tonnen emittieren und so Augsburgs Budget um den Faktor 3 überschreiten (siehe Anhang).

Einen großen Wald zu besitzen, hat recht wenig mit aktivem Handeln zu tun und der Blick in die Vergangenheit zeigt, dass es keine Leistung der aktuell regierenden CSU war, dass es heute so ist: Der Wald ist bereits seit Ewigkeiten in Augsburgs Besitz, und tatsächlich strebte 2007 die Stadtregierung einen Verkauf des Siebentischwalds an. Auch die Grünen haben sich in diesen Belangen nicht mit Ruhm bekleckert: Im Juli 2018 musste Rainer Erben, damals wie jetzt Umweltreferent der Stadtregierung, mit einer Eilklage davon abgehalten werden, in der Brutzeit Bäume am Herrenbach zu fällen. "Müssen wir die Regierung dafür loben, dass sie vorhandene Bäume nicht abholzt?", fragt Stoffels.

"Wenn Augsburg die viel zu hohen geplanten Emissionen aus der Atmosphäre ziehen wollte, müsste die Stadt das sechsfache der unbewaldeten Fläche Augsburgs aufforsten." Außerdem würde es mehrere Jahrzehnte dauern, bis dieser Wald die Treibhausgase aufgenommen hätte. Dies zeigt, dass Bewahrung von Wald als Klimaschutzmaßnahme völlig unzureichend ist.

Vor dem BR-Interview demonstrierte Frau Weber zuletzt in der Stadtratssitzung am 23. Juli, wie wichtig ihr die Klimakrise ist: Die Diskussion einer inhaltlich fundierten Beschlussvorlage von ‚Augsburg in Bürgerhand‘ zum sofortigen Beginn einer dezentralen Energiewende wurde auf nach der Sommerpause verschoben.

[1] https://www.br.de/mediathek/video/fridays-for-future-augsburg-ein-monat-klimacamp-av:5f259bdcac660f001abc5198 
[2] https://web.archive.org/web/20130205202943/http://www.ifw-kiel.de/wirtschaftspolitik/politikberatung/kiel-policy-brief/KPB_50.pdf
