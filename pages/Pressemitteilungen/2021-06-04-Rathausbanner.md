---
layout: page
title:  "4.6.2021: Klimacamper*innen hissen Banner bei Fahnenmasten auf dem Rathausplatz"
date:   2021-06-04 02:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 48
---

*Pressemitteilung vom Augsburger Klimacamp am 4. Juni 2021*

# Klimacamper\*innen hissen Banner bei Fahnenmasten auf dem Rathausplatz

Sperrfrist Freitag (4.6.2021) 18:30 Uhr
{: .label .label-red }

Am heutigen Freitagnachmittag kletterten Aktivist\*innen des Augsburger
Klimacamps die Fahnenmasten auf dem Rathausplatz hinauf, um dort in zehn Meter
Höhe ein Transparent zu befestigen, welches auf die nach Auffassung der
Klimacamper\*innen katastrophale Klimapolitik der Stadtregierung aufmerksam
macht. "Oberbürgermeisterin Weber führt eine Klimapolitik des Nichtstuns und
Abwartens an!", so Felicia Reintke (23).

Am gestrigen Donnerstagabend gab es bereits eine ähnliche Aktion, die schon
nach wenigen Minuten von Ordnungsamt und Polizei beendet wurde. "Es ist
bemerkenswert", so der angehende Förster Nico Kleitsch (21), "mit welcher
Energie die Stadt gegen Kritik an der CSU vorgeht". "Wenn sie nur ein
Zehntel so viel Energie auf Klimagerechtigkeit verwenden würde, hätten wir
schon lange unser Camp abbauen können", so Mathematik-Dozent Dr. Ingo
Blechschmidt (32).

Blechschmidt spielt damit auf die Klima-Sondersitzung des Stadtrats an: "Es ist
ungeheuerlich, dass sich die Stadtregierung im Jahr 2021 dafür feiert, nun als
letzte bayerische Großstadt einen Recyclingpapieranteil von 100% anzustreben."
Aktuell belegt Augsburg noch unter allen bayerischen Großstädten mit einem
Anteil von 48% den letzten Platz [1].

[1] https://www.nachhaltigkeit.augsburg.de/fileadmin/nachhaltigkeit/data/Indikatorenbl%C3%A4tter_Zukunftsleitlinien/Indikatorblatt_%C3%962.2_Papiereinkauf_und_Recyclingpapier.pdf

## Fotos zur freien Verwendung

https://www.speicherleck.de/iblech/stuff/.rathausbanner1
(dort sind momentan noch Fotos der Donnerstagsaktion hinterlegt, Fotos der
Freitagsaktion folgen gegen 19:00 Uhr -- die Banner am Freitag werden deutlich
höher hängen)

## Hinweis

Die Kletteraktion beginnt um 18:00 Uhr. Polizeiliche Intervention könnte die Aktion
verhindern.
