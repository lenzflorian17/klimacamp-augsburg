---
layout: page
title:  "10.08.2020: Jedes neue Autohaus offenbart das Versagen der Mobilitätspolitik"
date:   2020-08-10 07:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 9
---

*Pressemitteilung vom Augsburger Klimacamp am 10. August 2020*

# Protest anlässlich Eröffnung des neuen Autohauses in Lechhausen
## Jedes neue Autohaus offenbart das Versagen der Mobilitätspolitik

Anlässlich der Eröffnung des neuen Reisacher Autohauses gab es am heutigen Montagmorgen Protest von Klimagerechtigkeitaktivist\*innen. „Jedes neue Autohaus ist ein Symbol für das Versagen der Mobilitätspolitik“, erklärt Gesa Scholl (19) vom Augsburger Klimacamp ihr Engagement. „Viele Augsburger\*innen sind immer noch auf das Auto als Hauptverkehrsmittel angewiesen. Wenn Busse und Trams deutlich ausgebaut und deutlich günstiger würden, wäre es für alle leichter, klimafreundlicher zu leben.“

Mit dem Banner „Fahrradstadt jetzt“ lenkten die Aktivist\*innen daher die Aufmerksamkeit auf die Verantwortung der Stadtregierung hin, eine Mobilitätswende hin zu mehr öffentlichen Nahverkehr und dem Rad zu gestalten. „Innenstädte würden ruhiger und lebenswerter werden, mehr Raum für Menschen entstünde und ungemütliche Unorte würden verschwinden.“ Dass mehr Platz für den Menschen entsteht liegt vor allem daran, dass Autos durchschnittlich 23 Stunden pro Tag ungenutzt herumstehen und wenn sie doch verwendet werden meistens nur mit höchstens Personen. „Der Trend zu größeren Autos wird durch den Zwang der Profitmaximierung verstärkt“, ist sich Scholl bewusst.

In zweiter Linie diente der heutige Protest auch, um der Darstellung des neuen Autohauses als besonders klimafreundlich zu widersprechen. „Mit dem neuen Gebäude wurden unnötig neue Fläche versiegelt und klimaintensive Ressourcen verbraucht.“ Die beiden alten Gebäude, die durch das neue ersetzt werden, hätten weiterhin verwendet werden können. Damit widersprechen die Aktivist\*innen klar dem Narrativ von Reisacher, das die Umweltfreundlichkeit des Gebäudes in den Fokus gerückt hatte. „Hier wird mit Umweltfreundlichkeit geworben. Paradoxerweise bei einem Autohaus, dass immer noch zum größten Teil Verbrenner und viel zu große Autos verkauft. An der Umweltschädlichkeit dieser Autos ändert auch eine Solaranlage am Dach des Autohauses nichts.“ stellte Scholl klar.
