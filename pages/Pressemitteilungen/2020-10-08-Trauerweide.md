---
layout: page
title:  "08.10.2020: Augsburger Klimacamp sichert Frau Demmelmair volle Unterstützung zu und wird ihre Trauerweide verteidigen"
date:   2020-10-08 17:00:00 +0200
categories: jekyll update
parent: Pressemitteilungen
nav_order: 21
---

*Pressemitteilung vom Augsburger Klimacamp am 8. Oktober 2020*

# Augsburger Klimacamp sichert Frau Demmelmair volle Unterstützung zu und wird ihre Trauerweide verteidigen

Liebe Frau Demmelmair,

mit Bestürzung und Anteilnahme nahmen wir den Artikel in der Augsburger Allgemeinen [1] über die geplante Fällung Ihrer Trauerweide am Kaufbach zu Kenntnis. Wir können gut nachempfinden, dass Ihnen, den Bewohner:innen des Altenheims und den restlichen Anwohner:innen dieser prächtige Baum sehr wichtig ist. Auch wir sind mit Ihrem Baum vertraut, mehrere von uns verknüpfen liebe Kindheitserinnerungen mit ihm. Er bringt Leben in den sonst so grauen Weg an der Friedberger Straße. Wir können uns nur ausmalen, wie erfüllend es ist, einen Baum auf so lange Zeit zu begleiten und ihn kontinuierlich zu pflegen.

Leider kommt das Verhalten der Stadtregierung in diesem Punkt nicht überraschend: Dass unsere Stadtregierung unter Schirmherrschaft der CSU sowie Grünen-Umweltreferent Rainer Erben Bäume nicht richtig wertschätzt, hat leider Tradition. Etwa strebte sie 2007 einen Verkauf des Siebentischwalds an und beabsichtigte 2018, Bäume am Herrenbach zu fällen. Diese Zerstörungsvorhaben konnten nur durch ein von 'Augsburg in Bürgerhand' initiiertes Bürger:innenbegehren beziehungsweise eine Eilklage verhindert werden. Dabei ist jeder einzelne Baum wertvoll -- für die Lebensqualität der Anwohner:innen, für Insekten, für Abkühlung im Sommer, für bessere Luft und zur Bekämpfung der Klimakrise.

Unsere Stadtregierung ist dabei kein Einzelfall: In Hessen steht derzeit der Dannenröder Wald auf der Rodungsliste der Bundes- und Landesregierung. Dieser 300 Jahre alte Mischwald ist Naherholungs- und Wasserschutzgebiet, doch trotzdem soll er in Teilen einer neuen Autobahn weichen. Wir aus der Klimagerechtigkeitsbewegung sagen dazu: Das ist ein aus der Zeit gefallenes Vorhaben. Der Wald sollte für die Anwohner:innen erhalten bleiben, die Durchfahrtsstraßen sollten für den regionenübergreifenden Lieferverkehr gesperrt und die verplanten Steuergelder in den Ausbau des Schienennetzes investiert werden.

Wir versuchen, mit unserem zentralen Platz zwischen zwei historischen Gebäuden sorgsam umzugehen und ihn zu nutzen, um Aufmerksamkeit auf ein wichtiges Thema zu lenken. Das gelingt uns leider nicht immer. Wir schreiben Ihnen heute, um Ihnen mitzuteilen: Sie sind mit Ihrem Einsatz zur Rettung Ihres Baums nicht alleine. Neben der Baumallianz sichern auch wir Ihnen unsere volle Unterstützung zu. Sie können sich auf uns verlassen: Wir werden, wenn die Stadt zur Fällung anrückt, friedlich Ihren Baum verteidigen. Sie können uns dazu zu jeder Tages- und Nachtzeit unter den Telefonnummern +49 176 95110311 (Ingo Blechschmidt) und +49 162 6696097 (Alex Mai) erreichen. Bitte kontaktieren Sie uns, wenn es die Situation erfordert.

Um der Stadt zu demonstrieren, dass wir es mit dem Schutz Ihrer Trauerweide ernst meinen, werden wir sie am kommenden Samstag (10.10.2020) ab etwa 21:00 Uhr symbolisch für 24 Stunden besetzen. Wir freuen uns, wenn wir dabei ins Gespräch kommen und Sie aus dem Nähkästchen erzählen: Wie kam es damals dazu, dass Sie Ihre Trauerweide pflanzten? Ihr Engagement beeindruckt und inspiriert uns. Wir danken Ihnen herzlich dafür.

Liebe Grüße

Ihre Aktivist:innen des Augsburger Klimacamps

[1] https://www.augsburger-allgemeine.de/augsburg/Faellungen-am-Kaufbach-Diese-Augsburgerin-kaempft-um-ihren-Baum-id58242701.html
