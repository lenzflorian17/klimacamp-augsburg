---
layout: page
title: Kosten des Polizeieinsatzes im besetzten Dannenröder Wald
permalink: /danni/
nav_exclude: true
---

# 150 Mio. € + X € + 1 Toter? = Polizeieinsatz im Danni

*Letzte Aktualisierung:* 2.12.2020

## Überschlagsrechnung

Unter der Annahme, dass ca. <a href="https://www.hessenschau.de/panorama/ticker-a49-ausbau--dienstag-65-gegner-in-gewahrsam--polizei-aufrufe-im-internet-zu-straftaten--deges-besorgt-menschen-begeben-sich-in-gefahr--,dannenroeder-forst-ticker-100.html">2000 Polizist*innen</a> (<a href="https://www.fuldaerzeitung.de/hessen/dannenroeder-forst-a49-ausbau-giessen-kassel-protest-absturz-exkremente-polizei-zr-90095685.html">weitere Quelle</a>) jeden Tag im Einsatz (oder zumindest in Bereitschaft, also in der Umgebung einquartiert) sind, und wenn sie im Einsatz sind, dann in der Regel von morgens um ca. 6:00 Uhr bis abends um ca. 20:00 Uhr (Überlänge!), und unter der Verwendung der Schätzung der Polizeigewerkschaft, dass eine Hundertschaft -- <a href="https://www.wiwo.de/politik/deutschland/streit-um-hambacher-forst-polizeigewerkschaft-rechnet-mit-millionen-kosten-wegen-einsatz-im-hambacher-forst/23081452.html">entspricht 80 bis 120 Polizist*innen</a> -- ca. <a href="https://www.derwesten.de/region/hambacher-forst-rwe-polizei-id215364385.html">100.000 € pro Tag</a> im Einsatz kostet.
Dann kostet der Polizeieinsatz zur Rodung des Trinkwasser- und Naturschutzgebiets Dannenröder Wald, der vor 40 Jahren beschlossen wurde und heute so nicht mehr rechtens wäre, für eine Autobahn Steuerzahler\*innen <b>pro Tag gerundet 1,6 Mio. €.</b> Allein an Personalkosten!

Stichpunktartige Recherche ergab, dass aktuell alle Hotelanlagen im 30 km Umkreis des Dannenröder Waldes ausgebucht sind. Was in Zeiten von Corona eigentlich nur durch den Polizeieinsatz begründet sein kann. Aus Gesprächen mit Polizist\*innen erfuhren wir, dass sie Halbpension bekommen und fast alle Einzelzimmer haben. Hinzu kommt ein Lunchpaket als Mittagessen. Nehmen wir also an, pro Person werden 100 € für eine durchschnittliche Übernachtung veranschlagt, was eher konservativ gerechnet ist (immerhin sind ALLE Hotels ausgebucht), so kommen nochmals 200.000 € pro Nacht hinzu. Also sind wir bei 1,8 Mio. €.

Der Danni gilt vielen Waldbesetzer\*innen als die Weiterentwicklung des Hambacher Walds. Was sich zu bewahrheiten scheint, denn nach fast vier Wochen sind immer noch nicht alle Barrios (Baumhausdörfer) geräumt worden. Die Räumung des Hambi hat dagegen bei <a href="https://www.wiwo.de/politik/deutschland/streit-um-hambacher-forst-polizeigewerkschaft-rechnet-mit-millionen-kosten-wegen-einsatz-im-hambacher-forst/23081452.html">etwa gleichen vielen Polizist\*innen drei Wochen</a>  gedauert. Laut Gewerkschaft der Polizei entsprach das ca. <a href="https://ga.de/news/panorama/so-viele-stunden-war-die-polizei-im-hambacher-forst_aid-43916393">1 Mio. Arbeitsstunden</a>.

Hinzu kommt die <a href="https://www.youtube.com/watch?v=rDMJIx0jz44">lebensgefährliche und illegale Missachtung</a> der Sicherheitsbestimmung für die Forstwirtschaft, was zwar im Hambi auch vorkam, aber nicht so krass (nach bereits weniger als zwei Wochen Einsatz im Dannenröder Wald gab es schon <a href="https://anfdeutsch.com/Oekologie/eine-woche-der-rodungen-und-raumungen-im-dannenroder-wald-22804"><b>DREI von Polizist\*innen/Waldarbeiter\*innen verursachte Abstürze!</b></a> (weitere Quellen: <a href="https://anfdeutsch.com/Oekologie/dannenroder-forst-polizei-verursacht-erneut-schweren-unfall-22793">eins</a> und <a href="https://anfdeutsch.com/Oekologie/dannenroder-wald-aktivistin-sturzt-von-tripod-22774">zwei</a>).
Zuletzt stürzte am 21.11. eine Person so schwer, dass sie sich mehrfache Wirbelbrüche
zuzog (<a href="https://taz.de/Einsatz-im-Dannenroeder-Wald/!5727137/">Quelle</a>).
Würden die Abstands- und Sicherheitsregeln eingehalten, so wäre das Tempo der Rodung und Räumung deutlich geringer.

Nehmen wir an, die Polizei nimmt weiter billigend den Tod von Aktivisti in Kauf und behält ihr Räumungstempo bei, während der Zustrom an Aktivist\*innen weiter moderat zunimmt, so könnte die Polizei nach etwa fünf Wochen das letzte Baumhaus auf der Trasse geräumt haben. Auch danach wird der Konflikt nicht befriedet sein. Damit scheint auch die Polizei zu rechnen. Die Hotels in der Umgebung sind bis kurz vor Weihnachten weiterhin stark frequentiert, wenn auch nicht ausgebucht.

Der Polizeieinsatz im Hambi war <a href="https://www.sueddeutsche.de/politik/besetzter-wald-im-rheinischen-braunkohlerevier-polizei-raeumt-hambacher-forst-1.4127664">einer der größten Einsätze der bundesdeutschen Geschichte</a>. Der Einsatz im Danni wird ihn, das ist jetzt schon sicher, bei weitem an Dauer, Kosten und Personaleinsatz übertreffen &ndash; und das während dem Rest von Deutschland verboten wird, sich mit mehr als drei Menschen (aus verschiedenen Haushalten) zu treffen.

Rechnen wir also weiter mit <b>53 Tagen</b> (5 Wochen Danni (inkl. Wochenende), 3 Wochen Mauli+Herri (ohne Wochenenden)), hinzu kommen An- und Abreise und entsprechend mehr Übernachtungen. Aus verschiedenen stichpunktartigen Gesprächen mit Polizist\*innen erfuhren wir, dass sie zwischen drei und fünf Tagen bleiben und etwa einen halben Tag Anreise und einen halben Tag Abreise haben. Also kommen durchschnittlich auf vier Tage Einsatz fünf Übernachtungen sowie ein zusätzlicher Tag An-/Abreise.
Damit ergeben sich 8,75 Mio. € an Übernachtungs- und Verpflegungskosten.

So ergibt sich nach obiger Überlegung ein Kostenberg von etwa <b><i>146 Mio. €</i></b>. <b>Alleine für Personal und Unterbringung!</b>
Und das alles unter der Annahme, dass weder Corona, noch das Wetter, noch die zunehmende Mobilisierung der Besetzer\*innen, noch das plötzliche Eintreten von Vernunft seitens der Politik, noch der Tod eine\*r Aktivist\*in und dadurch die Einhaltung der Sicherheitsvorschriften einen Strich durch diese Rechnung macht.

## In dieser Überlegung sind nicht eingerechnet
* Kosten für technisches Gerät
* Kosten für Versorgung von Polizist\*innen und festgenommenen Aktivist\*innen
* <b>Kosten für die Kommunen in der Umgebung</b> - <a href="https://www1.wdr.de/nachrichten/rheinland/rwe-uebernahme-kosten-raeumung-hambach-100.html">beim Hambi-Einsatz <b>716.000 €</b></a>:<br>
    „Das hat am Dienstagnachmittag (17.12.2019) NRW-Heimatministerin Scharrenbach mitgeteilt. Dem Kreis Düren, der Gemeinde Merzenich und der Stadt Kerpen sind nach eigenen Angaben vom September 2019 insgesamt Kosten in Höhe von rund 716.000 Euro entstanden. Darunter fallen Ausgaben für Krisenstäbe und der Verdienstausfall der freiwilligen Einsatzkräfte von Feuerwehr und Rettungsdienst.“
* <b>Kosten durch Kriminalität durch die Abwesenheit von Polizist\*innen, da wo sie wirklich gebraucht werden.</b> <a href="https://taz.de/Anis-Amri-und-die-Rigaer-Strasse/!5510622/">Das kann auch mal tödlich enden, wie im Fall des Berlin-Attentäters Anis Amri</a>
* <b>Kosten durch die mögliche Verseuchung eines Trinkwasserschutzgebiets für 0,5 Mio.</b> Menschen, welches der Danni darstellt.
* <b>Kosten</b> durch die Erdaufheizung,
* <b>Politische Kosten für Die GRÜNEN,</b> deren hessischer Verkehrsminister diesen Wahnsinn gerade durchzieht, anstatt ein Moratorium zu beschließen, so wie es sein Recht und seine ethische Pflicht als grüner Verkehrsminister wäre. Spoiler: <a href="https://www.nzz.ch/international/dannenroeder-forst-entfremdung-zwischen-gruenen-und-aktivisten-ld.1583143">Kommt gar nicht gut bei der grünen Wählerschaft an!</a>

Nicht zuletzt ist es bei der aktuellen völligen Nichtberücksichtigung sämtlicher Sicherheitsvorschriften nur eine Frage der Zeit, <a href="https://www.youtube.com/watch?v=rDMJIx0jz44">bis Aktivist\*innen sterben</a>.


## Zum Vergleich

150 Mio. € ist mehr als das Doppelte der Parteispenden die CDU/CSU und Bündnis90/Die GRÜNEN 2019 erhalten haben, sondern würde auch reichen, um jedem der einige <a href="https://www.zeit.de/campus/2020-09/dannenroeder-forst-umweltschutz-klimaaktivismus-rodung-autobahnausbau">Hundert zählenden Besetzer\*innen</a> ein eigenes Haus zu kaufen.

Rechnet man alle zusätzlichen Kosten und die 150 Mio. € Personalkosten zusammen, so kommt man leicht auf eine Summe, die ausreicht um den kompletten <b>ÖPNV in Hessen</b> für die Zeit der Rodung (3,5 Monate) für alle Hess\*innen <b>kostenlos</b> zu machen!!! [Rechnung: 13,3 Milliarden Einnahmen im deutschen ÖPNV; Anteil der hessischen Bevölkerung an der deutschen Bevölkerung: 7%]

Der Beweis dafür, dass das wirklich unangebracht viel Geld ist, kommt ironischerweise von der CDU <a href="https://www.wz.de/politik/inland/warum-nrw-auf-kosten-fuer-die-raeumung-im-hambacher-forst-sitzen-bleibt_aid-37706641">persönlich</a>:

  <i>„In einer Vorlage der Landesregierung heißt es allerdings, über die Kosten für „technisches Gerät, Fachpersonal, Beseitigung von Bauschutt und Müll und die polizeilichen Maßnahmen“ gebe es „keine Vollkostenrechnung“. Der Grund: Öffentliche Sicherheit lasse sich nicht „als Wirtschaftsgut kommerzialisieren“, teilt die Ministerin Ina Scharrenbach (CDU) mit. Die Durchsetzung des Rechtsstaates habe keinen „Marktwert“. Und weiter: Die „konkrete Bemessung der Kosten eines Polizeieinsatzes“ führe dazu, „dass der Nutzen des Einsatzes anhand seiner Kosten beurteilt würde.“</i>

Jap, ihr habt richtig gehört. Die CDU (!!!) möchte nicht marktwirtschaftlich denken. Oder in anderen Worten, <b>der Einsatz war so teuer, dass es verdammt unangenehm wäre die Kosten zu veröffentlichen.</b>
