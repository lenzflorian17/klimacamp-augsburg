---
layout: page
title:  "02.10.2020: Erster Fragenkatalog an die Stadtwerke Augsburg"
date:   2020-10-02 02:00:00 +0200
categories: jekyll update
parent: "Offene Briefe"
nav_order: 2
---

# Erster Fragenkatalog an die Stadtwerke Augsburg am 2. Oktober 2020

*[Brief als PDF](/pages/material/Fragenkatalog-Stadtwerke/brief_swa_2020-10-02.pdf)*  
**[Zum Fragenkatalog](/pages/material/Fragenkatalog-Stadtwerke/fragenkatalog.pdf)**

Sehr geehrte Mitarbeiter\*innen bei den Stadtwerken Augsburg,

die Stadtwerke Augsburg nehmen in Augsburg eine wichtige Rolle
bei der Energie- und Mobilitätswende ein.
Als Augsburger Klimacamp sind wir daran interessiert,
ein möglichst vollständiges Bild
der notwendigen und möglichen Klimagerechtigkeitsmaßnahmen
in Augsburg und Umland zu erhalten.
Zu diesem Zweck haben wir einen Katalog von Fragen für Sie erstellt.

Bitte senden Sie uns die Antworten bis zum Abend des 13. Oktobers 2020
an die unten genannten E-Mail-Adressen zu.
Die erfragten Informationen sollen
Gespräche am Camp, in der Stadtpolitik und anderswo bereichern,
könnten Einfluss auf unsere Forderungen haben
und können vielleicht mit Vertreter\*innen der Stadtwerke Augsburg
bei zukünftigen Veranstaltung am Klimacamp persönlich
und im Detail erörtert werden.

Wir planen den beigelegten Fragenkatalog mitsamt Ihrer Antworten zu
kommentieren und zu veröffentlichen. Selbstverständlich hätten wir auch nichts
dagegen und würden es sogar begrüßen, wenn auch Sie die Fragen und Ihre
Antworten, etwa auf Ihrer eigenen Webseite, veröffentlichen würden. Sie
erhalten den Fragenkatalog auch per Mail.

Mit freundlichen Grüßen  
Ihre Aktivist\*innen des Augsburger Klimacamps
